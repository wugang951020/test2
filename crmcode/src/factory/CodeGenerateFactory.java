package factory;

import generate.Generate;
import generate.JavaGenerate;
import generate.JsGenerate;
import generate.JspGenerate;

import java.util.ArrayList;
import java.util.List;

import util.ConvertHandler;
import util.FileType;
import vo.Table;
import db.DBFactory;
import db.DataSource;

/**
 * 代码生成工厂
 * @ClassName: CodeGenerateFactory
 * @author: yeshengde
 * @date: 2017-1-9下午3:23:36
 */
public class CodeGenerateFactory {

    private static Generate gen;
    private static DataSource db;
    private static Table table;

    /**
     * 表格信息初始化
     * @param tableName  表名
     * @param className  对应的实体类名
     */
    public static void init(String tableName, String className,String remarks, String[] realNames) {
        try {
            table = new Table(tableName);
            table.setRemarks(remarks);
            db = DBFactory.create(db);
            db.getTable(tableName, className, table);
            //DBFactory.create(db);
            //db.getTable(tableName, className, table);
            ConvertHandler.convert(table, realNames);
            System.out.println("init:OK");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 创建实体类
     */
    public static void createEntity() {
        try {
            gen = new JavaGenerate(FileType.ENTITY);
            gen.generate(table);
            System.out.println("createEntity:OK");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建Model类
     */
    public static void createModel() {
        try {
            gen = new JavaGenerate(FileType.MODEL);
            gen.generate(table);
            System.out.println("createModel:OK");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建Dao
     */
    public static void createDao() {
        try {
            gen = new JavaGenerate(FileType.DAO);
            gen.generate(table);
            System.out.println("createDao:OK");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建Service
     */
    public static void createService() {
        try {
            gen = new JavaGenerate(FileType.SERVICE);
            gen.generate(table);
            System.out.println("createService:OK");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建serviceImpl
     */
    public static void createServiceImpl() {
        try {
            gen = new JavaGenerate(FileType.SERVICE_IMPL);
            gen.generate(table);
            System.out.println("createServiceImpl:OK");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建Controller
     */
    public static void createController() {
        try {
            gen = new JavaGenerate(FileType.CONTROLLER);
            gen.generate(table);
            System.out.println("createController:OK");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建Mapper
     */
    public static void createMapper() {
        try {
            gen = new JavaGenerate(FileType.MAPPER);
            gen.generate(table);
            System.out.println("createMapper:OK");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建JS
     */
    public static void createJs(String[] validates, String[] condJsonDatas) {
        try {
            gen = new JsGenerate(FileType.JS_SCRIPT);
            List<String> condJsonDataList = new ArrayList<String>();
            List<String> validateList = new ArrayList<String>();
            for (String condJsonData : condJsonDatas) {
                condJsonDataList.add(condJsonData);
            }
            for (String validate : validates) {
                validateList.add(validate);
            }
            table.setCondJsonData(condJsonDataList);
            table.setValidate(validateList);
            gen.generate(table);
            System.out.println("createJs:OK");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建JSP
     */
    public static void createJsp() {
        try {
            gen = new JspGenerate(FileType.JSP_PAGE);
            gen.generate(table);
            System.out.println("createPage:OK");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void createRepository() {
        try {
            gen = new JavaGenerate(FileType.REPOSITORY);
            gen.generate(table);
            System.out.println("createEntity:OK");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
