package util;

/**
 * 文件相关信息：包名，包路径，模版路径，文件扩展名
 * @author yeshengde
 * @since 2016-06-03
 */

public enum FileType {

    ENTITY("entity", Config.PACKAGE_ENTITY, Config.TEMPLATE_ENTITY, ".java"), MODEL("model", Config.PACKAGE_MODEL,
            Config.TEMPLATE_MODEL, "Example.java"), DAO("dao", Config.PACKAGE_DAO, Config.TEMPLATE_DAO, "Mapper.java"), MAPPER(
            "mapper", Config.PACKAGE_MAPPER, Config.TEMPLATE_MAPPER, "Mapper.xml"), SERVICE("service",
            Config.PACKAGE_SERVICE, Config.TEMPLATE_SERVICE, "Service.java"), SERVICE_IMPL("service.impl",
            Config.PACKAGE_SERVICE + ".impl", Config.TEMPLATE_SERVICE_IMPL, "ServiceImpl.java"), CONTROLLER(
            "controller", Config.PACKAGE_CONTROLLER, Config.TEMPLATE_CONTROLLER, "Controller.java"), JS_SCRIPT(
            "controller", Config.PACKAGE_JS, Config.TEMPLATE_JS, ".js"), JSP_PAGE("controller", Config.PACKAGE_JSP,
            Config.TEMPLATE_JSP, ".jsp"), REPOSITORY("repository", Config.PACKAGE_REPOSITORY,
            Config.TEMPLATE_REPOSITORY, "Repository.java");

    private String type;
    private String path;
    private String template;
    private String fileNameExtension;

    private FileType(String type, String path, String template, String fileNameExtension) {
        this.type = type;
        this.path = path;
        this.template = template;
        this.fileNameExtension = fileNameExtension;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getFileNameExtension() {
        return fileNameExtension;
    }

    public void setFileNameExtension(String fileNameExtension) {
        this.fileNameExtension = fileNameExtension;
    }

}
