package util;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.velocity.VelocityContext;

import vo.Table;

public final class Config {

    /*************数据库配置********************/
    public static String DATABASE_CHOOSE = "";
    public static String DRIVER = "";
    public static String USERNAME = "";
    public static String PASSWORD = "";
    public static String URL = "";

    /*****作者******/
    public static String AUTHOR = "";
    /************ 模板配置 ************/
    public static String NEW_PROJECT = "";
    public static String TEMPLATE_PATH = "";
    public static String TEMPLATE_ENTITY = "";
    public static String TEMPLATE_MODEL = "";
    public static String TEMPLATE_DAO = "";
    public static String TEMPLATE_MAPPER = "";
    public static String TEMPLATE_SERVICE = "";
    public static String TEMPLATE_SERVICE_IMPL = "";
    public static String TEMPLATE_CONTROLLER = "";
    public static String TEMPLATE_REPOSITORY = "";

    public static String TEMPLATE_JS = "";
    public static String TEMPLATE_JSP = "";

    /******************文件路径****************/
    public static String JAVA_STORE_PATH = "";
    public static String JS_STORE_PATH = "";
    public static String JSP_STORE_PATH = "";
    /*****************文件包配置********************************/
    public static String PACKAGE_NAME = "";
    public static String PACKAGE_SUFFIX = "";
    public static String PACKAGE_BASE = "";
    public static String PACKAGE_ENTITY = "";
    public static String PACKAGE_MODEL = "";
    public static String PACKAGE_DAO = "";
    public static String PACKAGE_MAPPER = "";
    public static String PACKAGE_SERVICE = "";
    public static String PACKAGE_CONTROLLER = "";
    public static String PACKAGE_JS = "";
    public static String PACKAGE_JSP = "";
    public static String PACKAGE_REPOSITORY = "";
    
    /***************路径位置*********************/
    public static String DAO_PACKAGE;
    public static String ENTITY_PACKAGE;
    public static String SERVICE_PACKAGE;
    public static String CONTROLLER_PACKAGE;
    public static String EXAMPLE_PACKAGE;
    public static String special_String;
    static {
        Properties prop = new Properties();
        InputStream in = Config.class.getResourceAsStream("/config/config.properties");
        try {
            prop.load(in);
            DATABASE_CHOOSE = prop.getProperty("database.choose").trim();
            //数据库信息配置
            DRIVER = prop.getProperty(DATABASE_CHOOSE + ".driver").trim();
            URL = prop.getProperty(DATABASE_CHOOSE + ".url").trim();
            USERNAME = prop.getProperty(DATABASE_CHOOSE + ".username").trim();
            PASSWORD = prop.getProperty(DATABASE_CHOOSE + ".password").trim();

            AUTHOR = prop.getProperty("author").trim();
            //模版路径配置
            TEMPLATE_PATH = prop.getProperty("template_path").trim();
            TEMPLATE_ENTITY = prop.getProperty("template_entity").trim();
            TEMPLATE_MODEL = prop.getProperty("template_model").trim();
            TEMPLATE_DAO = prop.getProperty("template_dao").trim();
            NEW_PROJECT = prop.getProperty("new_project").trim();
            if (NEW_PROJECT.equals("true")) {
                TEMPLATE_MAPPER = prop.getProperty("template_mapper").trim();
            } else {
                TEMPLATE_MAPPER = "java_mapper.vm";
            }
            TEMPLATE_SERVICE = prop.getProperty("template_service").trim();
            TEMPLATE_SERVICE_IMPL = prop.getProperty("template_service_impl").trim();
            TEMPLATE_CONTROLLER = prop.getProperty("template_controller").trim();
            TEMPLATE_REPOSITORY = prop.getProperty("template_repository").trim();
            JAVA_STORE_PATH = prop.getProperty("java_store_path").trim();
            JS_STORE_PATH = prop.getProperty("js_store_path").trim();
            JSP_STORE_PATH = prop.getProperty("jsp_store_path").trim();
            TEMPLATE_JS = prop.getProperty("template_js").trim();
            TEMPLATE_JSP = prop.getProperty("template_jsp").trim();
            //初始化文件包路径
            PACKAGE_NAME = prop.getProperty("package_name").trim();
            PACKAGE_SUFFIX = prop.getProperty("package_suffix").trim();
            PACKAGE_ENTITY = prop.getProperty("package_entity").trim().replace("?", PACKAGE_NAME)
                    .replace("*", PACKAGE_SUFFIX);
            PACKAGE_MODEL = prop.getProperty("package_model").trim().replace("?", PACKAGE_NAME)
                    .replace("*", PACKAGE_SUFFIX);
            PACKAGE_DAO = prop.getProperty("package_dao").trim().replace("?", PACKAGE_NAME)
                    .replace("*", PACKAGE_SUFFIX);
            PACKAGE_MAPPER = prop.getProperty("package_mapper").trim().replace("?", PACKAGE_NAME)
                    .replace("*", PACKAGE_SUFFIX);
            PACKAGE_SERVICE = prop.getProperty("package_service").trim().replace("?", PACKAGE_NAME)
                    .replace("*", PACKAGE_SUFFIX);
            PACKAGE_CONTROLLER = prop.getProperty("package_controller").trim().replace("?", PACKAGE_NAME)
                    .replace("*", PACKAGE_SUFFIX);
            PACKAGE_JS = prop.getProperty("package_js").trim().replace("?", PACKAGE_NAME).replace("*", PACKAGE_SUFFIX);
            PACKAGE_JSP = prop.getProperty("package_jsp").trim().replace("?", PACKAGE_NAME)
                    .replace("*", PACKAGE_SUFFIX);
            PACKAGE_REPOSITORY = prop.getProperty("package_repository").trim().replace("?", PACKAGE_NAME)
                    .replace("*", PACKAGE_SUFFIX);
            DAO_PACKAGE = prop.getProperty("dao_package").trim();
            ENTITY_PACKAGE = prop.getProperty("entity_package").trim();
            SERVICE_PACKAGE = prop.getProperty("service_package").trim();
            CONTROLLER_PACKAGE = prop.getProperty("controller_package").trim();
            EXAMPLE_PACKAGE = prop.getProperty("example_package").trim();
            special_String = prop.getProperty("special_String").trim();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static VelocityContext getContext(Table table) {
        VelocityContext context = new VelocityContext();
        context.put("tableName", table.getTableName().toLowerCase());
        context.put("tableNameReal", table.getTableName());
        context.put("daoPackage", Config.DAO_PACKAGE);
       // context.put("mapperPackage", Config.DAO_PACKAGE);
        context.put("entityPackage", Config.ENTITY_PACKAGE);
        //context.put("modelPackage", Config.ENTITY_PACKAGE);
        context.put("packageName", Config.PACKAGE_NAME);
        context.put("servicePackage", Config.SERVICE_PACKAGE);
        context.put("specialString", Config.special_String);
        context.put("examplePackage", Config.EXAMPLE_PACKAGE);
        context.put("controllerPackage", Config.CONTROLLER_PACKAGE);
        context.put("jsPackage", Config.PACKAGE_JS);
        context.put("jspPackage", Config.PACKAGE_JSP);
        context.put("packageSuffix", Config.PACKAGE_SUFFIX);
        context.put("author", Config.AUTHOR);
        context.put("createTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        context.put("basePage.offset", "#{basePage.offset}");
        context.put("basePage.limit", "#{basePage.limit}");
        context.put("basePage.sort", "${basePage.sort}");
        context.put("basePage.order", "${basePage.order}");
        context.put("offset", "#{offset}");
        context.put("limit", "#{limit}");
        context.put("sort", "${sort}");
        context.put("order", "${order}");
        context.put("key", "${key}");
        context.put("value", "value");
        context.put("isOk", "isOk");
        context.put("id", "id");

        return context;
    }
}
