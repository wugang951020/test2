import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.util.Properties;

import vo.Column;

public class Test {
	public static void main(String[] args) throws ParseException, Exception {
		Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();

		Properties props = new Properties();
		props.setProperty("user", "CBA");
		props.setProperty("password", "chinaoly");
		
		Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.16.154:1521:orcl", props);
		
		Statement statement = conn.createStatement();
		ResultSet rs = statement.executeQuery("select * from QB_RYHX_AJ_ENTRY t");
		while(rs.next()){
			System.out.println(rs.getString("AJBH"));
		}
		
		DatabaseMetaData dmd = conn.getMetaData();
		rs = dmd.getColumns(null, null, "QB_RYHX_AJ_ENTRY", null);
		
		ResultSet rs2 = dmd.getPrimaryKeys(null, null, "QB_RYHX_AJ_ENTRY");
		while (rs2.next()) {
			System.out.println(rs2.getString("COLUMN_NAME"));
		}
		while (rs.next()) {
			Column col = new Column();
			col.setName(rs.getString("COLUMN_NAME"));
			col.setType(rs.getString("TYPE_NAME"));
			col.setSize(rs.getInt("COLUMN_SIZE"));
			col.setNullable(rs.getBoolean("NULLABLE"));
			col.setDigits(rs.getInt("DECIMAL_DIGITS"));
			col.setDefaultValue(rs.getString("COLUMN_DEF"));
			col.setComment(rs.getString("REMARKS"));
			System.out.println(col);
		}
	}
}
