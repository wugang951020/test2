package generate;

import java.util.List;

import vo.Table;

/**
 * 生成代码和创建文件接口
 * @author qhluo
 * @since 2016-05-13
 */
public interface Generate {

	public void generate(Table table) throws Exception;

	public void generate(List<Table> tables) throws Exception;

}
