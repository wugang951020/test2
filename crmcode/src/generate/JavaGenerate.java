package generate;

import java.io.StringWriter;
import java.util.List;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;

import util.Config;
import util.FileType;
import util.FileUtil;
import util.VelocityUtil;
import vo.Column;
import vo.Table;

/**
 * 鐢熸垚java鏂囦欢鎴栬�卪ybatis鏄犲皠鏂囦欢
 * 
 * @author qhluo
 * @since 2016-05-13
 *
 */

public class JavaGenerate implements Generate {

    FileType javaFileType;

	public JavaGenerate(FileType javaFileType) {
		this.javaFileType = javaFileType;
	}

    @Override
    public void generate(Table table) throws Exception {
        if (javaFileType == null) {
            throw new Exception(JavaGenerate.class.getName() + ":javaFileType is null !");
        }

        //get the template
        Template template = VelocityUtil.getTemplate(javaFileType.getTemplate());
        //created a context and add datasrc

        VelocityContext context = Config.getContext(table);
        //璁剧疆鏂囦欢鐩稿叧淇℃伅
        String className = table.getClassName();
        String entityInstance = className.substring(0, 1).toLowerCase() + className.substring(1);
        String mapperEntityInstance = className.substring(0, 4).toLowerCase();
        context.put("entityName", className);
        context.put("entityInstance", entityInstance); 
        context.put("mapperEntityInstance", mapperEntityInstance);
        List<Column> list= table.getColumns();
        context.put("columns", list);
        context.put("remark", table.getRemarks());
        context.put("pk", table.getPk());

        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        System.out.println(Config.JAVA_STORE_PATH + "/" + javaFileType.getPath().replaceAll("\\.", "/"));
        System.out.println(className + javaFileType.getFileNameExtension());
        FileUtil.create(Config.JAVA_STORE_PATH + "/" + javaFileType.getPath().replaceAll("\\.", "/"), className
                + javaFileType.getFileNameExtension(), writer.toString());

    }

    @Override
    public void generate(List<Table> tables) throws Exception {
        // TODO Auto-generated method stub

    }

}
