/**
 * 项目名称:统一后台管理
 *
 * vo
 *
 * TableName.java
 *
 * 2017年7月25日-上午10:52:07
 *
 * 2017 量化全球（北京）科技有限公司--杭州-版权所有
 */

package vo;


/**
 * <p>TableName</p>
 *
 * <p>描述：</p>
 *
 * <p>备注：</p>
 *
 * <p>2017年7月25日 上午10:52:07</p>
 *
 * @author yeshengde
 *
 * @version 1.0.0
 *
 */

public class TableName {

    public String dbTable;
    public String entityName;
    public String remark;
    
    /**
     * <p> 属性dbTable的Getter方法. </p>
     *
     * @return 返回dbTable属性的值
     */
    public String getDbTable() {
        return dbTable;
    }
    
    /**
     * <p> 属性dbTable的Setter方法. </p>
     *
     * @param dbTable 为属性dbTable设置的值
     */
    public void setDbTable(String dbTable) {
        this.dbTable = dbTable;
    }
    
    /**
     * <p> 属性entityName的Getter方法. </p>
     *
     * @return 返回entityName属性的值
     */
    public String getEntityName() {
        return entityName;
    }
    
    /**
     * <p> 属性entityName的Setter方法. </p>
     *
     * @param entityName 为属性entityName设置的值
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }
    
    /**
     * <p> 属性remark的Getter方法. </p>
     *
     * @return 返回remark属性的值
     */
    public String getRemark() {
        return remark;
    }
    
    /**
     * <p> 属性remark的Setter方法. </p>
     *
     * @param remark 为属性remark设置的值
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
    
}
