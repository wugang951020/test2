import java.util.ArrayList;
import java.util.List;

import factory.CodeGenerateFactory;
import vo.TableName;

/**
 * 鍒涘缓浠ｇ爜锛屽叿浣撻厤缃湪src/config/config.properties涓�
 * 
 * @ClassName: CreateCode
 * @author: yeshengde
 * @date: 2017-3-11涓嬪崍3:22:02
 */
public class CreateCode {

	public static void main(String[] args) {
		List<TableName> list = new ArrayList<TableName>();
		TableName tableName4 = new TableName();
		tableName4.setDbTable("qb_prize");
		tableName4.setEntityName("Prize");
		tableName4.setRemark("奖品表");
		list.add(tableName4);
		create(list);
	}

	public static void create(List<TableName> list) {
		for (TableName tableName : list) {
			CodeGenerateFactory.init(tableName.getDbTable(), tableName.getEntityName(), tableName.getRemark(), null);
			//CodeGenerateFactory.createDao(); // mapper
			CodeGenerateFactory.createEntity(); // entity
			CodeGenerateFactory.createModel();// example
			CodeGenerateFactory.createMapper(); // xml
		}

	}

}
